import datetime
import time
import pricing
import math

__version__ = '0.2'
__author__ = 'Nick Heaphy'
__home_url__ = "https://bitbucket.org/nickheaphy/f-i300-log-parser"

__csvkeys__ = 227
# Note this not that good as there are actually 17 medias parameters, but should be OK
# as assume if one name is changed then all will be...
__fieldsused__ = ['jobname',
                    'activetime',
                    'idletime',
                    'nofprinteda4bw',
                    'nofprinteda4c',
                    'nofprinteda3bw',
                    'nofprinteda3c',
                    'inkcolorcyan',
                    'inkcolormagenta',
                    'inkcoloryellow',
                    'inkcolorblack',
                    'inkblack',
                    'colorgrip',
                    'nofsimplex1',
                    'nofduplex1',
                    'mediaformat1',
                    'mediatype1',
                    'mediacolor1',
                    'mediaweight1',
                    'medianame1',
                    'cyclelength1',
                    'isinsert1',
                    'istab1'
                ]

def validate_csv(csv_dict):
    '''Check to see if csv_dict is valid and contains the data we expect'''
    # First check the number of columns of data
    if len(csv_dict.fieldnames) != __csvkeys__:
        print("This CSV contains %s keys but I expected %s" % (len(csv_dict.fieldnames), __csvkeys__))
        print("Please contact %s" % __author__)
        exit(1)
    
    # Check the data that we extract
    if not set(__fieldsused__).issubset(set(csv_dict.fieldnames)):
        print("Sorry, this CSV does not appear to contain all the columns we need.")
        print("Please contact %s" % __author__)
        exit(1)


class Job_i300(object):
    '''Properties of a job'''

    class PaperUsage(object):
        '''Info about paper used within a job'''

        class Media(object):
            def __init__(self):
                self.nofsimplex = 0
                self.noduplex = 0
                self.mediaformat = ""
                self.mediatype = ""
                self.mediaweight = ""
                self.mediacolor = ""
                self.medianame = ""
                self.cyclelength = 0
                self.isinsert = ""
                self.istab = ""


        def __init__(self):
            self.medialist = []
        
        def add_media(self,nofsimplex,nofduplex,mediaformat,mediatype,mediaweight,mediacolor,medianame,cyclelength,isinsert,istab):
            '''add a media'''
            # check that there is data
            if medianame != "":
                media = self.Media()
                media.nofsimplex = int(nofsimplex)
                media.nofduplex = int(nofduplex)
                media.mediaformat = mediaformat
                media.mediatype = mediatype
                media.mediaweight = mediaweight
                media.medianame = medianame
                media.cyclelength = int(cyclelength)
                media.isinsert = isinsert
                media.istab = istab

                self.medialist.append(media)


    # --------------------------------------


    # --------------------------------------
    class InkVolume(object):
        '''Hold Information about ink usage'''

        # --------------------------------------
        class Ink(object):
            '''Hold information about an ink'''
            def __init__(self, price):
                self.volume_ul = 0.0
                self.price_per_litre = price

            def return_ink_price(self):
                return self.convert_ul_to_l(self.volume_ul) * self.price_per_litre
            
            def return_volume_l(self):
                return self.convert_ul_to_l(self.volume_ul)

            @staticmethod
            def convert_ul_to_l(volume_ul):
                return volume_ul / 1000000.0
        # --------------------------------------

        def __init__(self):
            '''Internally these are stored in ul (microlitres)'''
            self.ink = {
                "cyan" : self.Ink(pricing.c_price_per_l),
                "magenta" : self.Ink(pricing.m_price_per_l),
                "yellow" : self.Ink(pricing.y_price_per_l),
                "black" : self.Ink(pricing.k_price_per_l),
                "blackonly" : self.Ink(pricing.k_price_per_l),
                "colourgrip" : self.Ink(pricing.cg_price_per_l)
            }
        
        def return_total_price(self):
            total_price = 0.0
            for k, each_ink in self.ink.items():
                total_price += each_ink.return_ink_price()
            return total_price
    
    # --------------------------------------

    def __init__(self):
        self.inkusage = self.InkVolume()
        self.paper_usage = self.PaperUsage()

    # Conversion Functions
    @staticmethod
    def convert_txt_to_datetime(txt_to_convert):
        '''convert a string in H:M:S format to datetime'''
        return time.strptime(txt_to_convert,'%H:%M:%S')

    @staticmethod
    def convert_datetime_to_minutes(dt_2_conv):
        '''convert a datetime to fractional minutes'''
        return float(datetime.timedelta(hours=dt_2_conv.tm_hour,minutes=dt_2_conv.tm_min,seconds=dt_2_conv.tm_sec).total_seconds()/60)

    @staticmethod
    def convert_minutes_to_hms(minutes):
        return str(datetime.timedelta(seconds=minutes*60))

    def a4_total_count(self):
        '''return total A4 equivalent clicks'''
        return self.a4_mono_total + self.a4_cmyk_total + self.a3_mono_total * 2 + self.a3_cmyk_total * 2
    
    def a4_speed(self):
        if self.activetime_min > 0:
            return math.ceil(self.a4_total_count() / self.activetime_min)
        else:
            return 0
    
    # Pricing Calculations

    def a4_mono_price(self):
        return self.a4_mono_total * pricing.a4_mono_click

    def a4_cmyk_price(self):
        return self.a4_cmyk_total * pricing.a4_cmyk_click
    
    def a3_mono_price(self):
        return self.a3_mono_total * pricing.a3_mono_click

    def a3_cmyk_price(self):
        return self.a3_cmyk_total * pricing.a3_cmyk_click

    def click_total_price(self):
        return self.a4_mono_price() + self.a4_cmyk_price() + self.a3_mono_price() + self.a3_cmyk_price()

    def total_price(self):
        return self.click_total_price() + self.inkusage.return_total_price()


    def parse_data(self,csv_dict):
        '''parse the CSV data out into job properties'''
        self.jobname = csv_dict['jobname']
        self.activetime_min = self.convert_datetime_to_minutes(self.convert_txt_to_datetime(csv_dict['activetime']))
        self.idletime_min = self.convert_datetime_to_minutes(self.convert_txt_to_datetime(csv_dict['idletime']))
        self.a4_mono_total = int(csv_dict['nofprinteda4bw'])
        self.a4_cmyk_total = int(csv_dict['nofprinteda4c'])
        self.a3_mono_total = int(csv_dict['nofprinteda3bw'])
        self.a3_cmyk_total = int(csv_dict['nofprinteda3c'])

        self.inkusage.ink["cyan"].volume_ul = int(csv_dict['inkcolorcyan'])
        self.inkusage.ink["magenta"].volume_ul = int(csv_dict['inkcolormagenta'])
        self.inkusage.ink["yellow"].volume_ul = int(csv_dict['inkcoloryellow'])
        self.inkusage.ink["black"].volume_ul = int(csv_dict['inkcolorblack'])
        self.inkusage.ink["blackonly"].volume_ul = int(csv_dict['inkblack'])
        self.inkusage.ink["colourgrip"].volume_ul = int(csv_dict['colorgrip'])

        # Medias
        for i in range(1,17):
            # nofsimplex,nofduplex,mediaformat,mediatype,mediaweight,mediacolor,medianame,cyclelength,isinsert,istab
            self.paper_usage.add_media(
                csv_dict['nofsimplex' + str(i)],
                csv_dict['nofduplex' + str(i)],
                csv_dict['mediaformat' + str(i)],
                csv_dict['mediatype' + str(i)],
                csv_dict['mediacolor' + str(i)],
                csv_dict['mediaweight' + str(i)],
                csv_dict['medianame' + str(i)],
                csv_dict['cyclelength' + str(i)],
                csv_dict['isinsert' + str(i)],
                csv_dict['istab' + str(i)],
            )
        



