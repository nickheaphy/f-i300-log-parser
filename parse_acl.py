#!/usr/local/bin/python3
from __future__ import print_function

# Parse an i300 ACL (or CSV) and do some reporting
__version__ = '0.2'
__author__ = 'Nick Heaphy'
__home_url__ = "https://bitbucket.org/nickheaphy/f-i300-log-parser"

import sys
import csv
import os.path
import pricing
import datetime
import time
import Job_i300_Class

PAGELIMIT = 500

class terminal_colour:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'


def main(csv_to_open):

    __csvfolder__, __csvfile__ = os.path.split(os.path.abspath(sys.argv[1]))

    with open(csv_to_open, 'r', encoding='utf-8-sig') as csvfile:
        #reader = csv.reader(csvfile, delimiter=';', quotechar='"')
        reader = csv.DictReader(csvfile, delimiter=';', quotechar='"')
        
        # Validate the CSV
        Job_i300_Class.validate_csv(reader)

        joblist = []

        for row in reader:
            job = Job_i300_Class.Job_i300()
            job.parse_data(row)
            joblist.append(job)
            

        for job in joblist:
            if job.a4_total_count() < PAGELIMIT:
                continue
            
            print("%s" % (job.jobname))
            print("--------------------")
            print("Clicks:")
            print(" A4 Mono: %s clicks at $%s = $%.2f" % (job.a4_mono_total, pricing.a4_mono_click, job.a4_mono_price()))
            print(" A4 CMYK: %s clicks at $%s = $%.2f" % (job.a4_cmyk_total, pricing.a4_cmyk_click, job.a4_cmyk_price()))
            print(" A3 Mono: %s clicks at $%s = $%.2f" % (job.a3_mono_total, pricing.a3_mono_click, job.a3_mono_price()))
            print(" A3 CMYK: %s clicks at $%s = $%.2f" % (job.a3_cmyk_total, pricing.a3_cmyk_click, job.a3_cmyk_price()))
            print(" Click Total: $%.2f" % (job.click_total_price()))
            
            print("Ink:")
            # Ink Usage
            for k, v in job.inkusage.ink.items():
                print(" %s Ink: %s l at $%.2f/l = $%.2f" %
                    (
                        str(k).capitalize(),
                        v.return_volume_l(),
                        v.price_per_litre,
                        v.return_ink_price()
                    ))
            print(" Ink Total Price: $%.2f" % (job.inkusage.return_total_price()))

            print("Total Price: $%.2f" % (job.total_price()))
            print(terminal_colour.BOLD, end='')
            print("Traditional A4 Click (Ink+Service): $%.5f" % (job.total_price() / job.a4_total_count()))
            print(terminal_colour.END, end='')

            print("Medias Used: %s" % (len(job.paper_usage.medialist)))
            for media in job.paper_usage.medialist:
                print(" %s (%s * %s)" % (media.medianame, int(media.nofsimplex + media.nofduplex/2), media.mediaformat))
            print(" Equivalent A4: %s" % (job.a4_total_count()))
            print("Time:")
            print(" Active time: %s" % (job.convert_minutes_to_hms(job.activetime_min)))
            print(" Idle time: %s" % (job.convert_minutes_to_hms(job.idletime_min)))
            print(" Speed: %.0f A4ipm" % (job.a4_speed()))
            print("")
            print("")

    print("Note: Data is based on 'stacked' pages (ie pages that have passed successfully through the engine)")
    print("No paper, power, waste ink or fixed service costs included")
    print("Minimum A4 page count for reporting: %s A4 equivalent" % PAGELIMIT)




if __name__ == "__main__":
# Check the passed parameters
    if len(sys.argv) != 2:
        print("Need to pass the ACL/CSV file on the commandline")
        exit(1)
    
    if sys.version_info[0] != 3:
        print("Sorry, needs Python 3 to handle Unicode ACL/CSV files")
        exit(1)
    
    if not os.path.isfile(sys.argv[1]):
        print("Can't open the ACL/CSV file")
        exit(1)

    main(sys.argv[1])
