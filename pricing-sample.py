#!/usr/local/bin/python3

# Ink Pricing
c_price_kg  = 9999
m_price_kg  = 9999
y_price_kg  = 9999
k_price_kg  = 9999
cg_price_kg = 9999

# Click Pricing
a4_mono_click_per_1000 = 9999
a4_cmyk_click_per_1000 = 9999


# ---------------------
# Conversions

# 5kg Bottle Volumes
c_volume  = 4.7
m_volume  = 4.7
y_volume  = 4.7
k_volume  = 4.7
cg_volume = 4.1

# Price per l
c_price_per_l  = c_price_kg * 5  / c_volume
m_price_per_l  = m_price_kg * 5  / m_volume
y_price_per_l  = y_price_kg * 5  / y_volume
k_price_per_l  = k_price_kg * 5  / k_volume
cg_price_per_l = cg_price_kg * 5 / cg_volume

a4_mono_click = a4_mono_click_per_1000 / 1000
a4_cmyk_click = a4_cmyk_click_per_1000 / 1000
a3_mono_click = a4_mono_click * 2
a4_cmyk_click = a4_cmyk_click * 2