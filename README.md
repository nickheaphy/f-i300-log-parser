# Simple Tools to Calculate costs from Oce Varioprint i300

Some basic Python scripts to convert the CSV/ACL accounting log files from Oce Varioprint i300 into dollar figures.

It is a work in progress...

## Installation

The scripts do require Python 3 as Python 2 does not handle UTF encoded CSVs very well.

### Mac OSX

On OSX Apple only ship Python 2.7 so you need to install Python 3. This is reasonably straightforward using `HomeBrew` https://brew.sh/

First install Brew by entering the following in Terminal `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`

Then once installed enter the following in Terminal `brew install python3`

Download the [repository](https://bitbucket.org/nickheaphy/f-i300-log-parser/get/master.zip) to a folder.

### Windows

Download and install the latest Python 3 from https://www.python.org/downloads/windows/

Download the [repository](https://bitbucket.org/nickheaphy/f-i300-log-parser/get/master.zip) to a folder.

## Downloading the i300 logs

Visit `http://<ip-of-i300>/accounting` to get the previous day's CSV or current days ACL file.

## Using the script

Edit the `pricing-sample.py` to include the actual costs and save as pricing.py

`python3 parse_acl.py accounting_log_file.acl`